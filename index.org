#+TITLE: Flow over a Wavy Wall


* Amazing databases:
Really want to investigate this later
Links to Other Online Databases and Collections

    Classic Case Studies http://cfd.mace.manchester.ac.uk/ercoftac/doku.php?id=database-links
    AGARD Report https://torroja.dmt.upm.es/turbdata/agard/
    John Hopkins Turbulence Databases http://turbulence.pha.jhu.edu/
    Turbulence and Heat Transfer Lab., University of Tokyo Databases https://thtlab.jp/
    NASA Langley Research Center, Turbulence Modeling Resource https://turbmodels.larc.nasa.gov/index.html
    The University of Texas at Austin, Turbulence Database https://turbulence.oden.utexas.edu/
    Univ. Politecnica de Madrid, Fluid Dynamics Group Turbulence Database https://torroja.dmt.upm.es/turbdata/
    KTH Stockholm, Turbulence Data from Philipp Schlatter https://www.mech.kth.se/~pschlatt/DATA/
    Tokyo University of Science, Kawamura Lab DNS Database of Wall Turbulence and Heat Transfer https://www.rs.tus.ac.jp/t2lab/db/
    TU-Darmstadt FDY DNS database https://www.fdy.tu-darmstadt.de/fdyresearch/dns/direkte_numerische_simulation.en.jsp
    TurBase knowledge-base for turbulence data https://turbase.cineca.it

https://thtlab.jp/
Loads of channel flows

* Experimental data for validation

# From the classic collection database
# http://cfd.mace.manchester.ac.uk/ercoftac/doku.php?id=cases:case076

Measurements of the spatial and time variation of two components of the velocity have been made over a sinusoidal solid wavy boundary with a height to length ratio of 2δ/λ=0.10 and with a dimensionless wave number of α+=(2π/λ)(ν/u∗)=0.02

The experiment was made in a rectangular water channel. The test section was preceded by a developing section of about 100 channel heights. The lower wall was constructed with removable Plexiglas plates on which the waves were milled. The LDV measurements were made above the 31st of 36 waves to ensure that a fully periodic flow had developed.

# So we kinda need to do the same thing - 36 waves

For corresponding DNS results see Case 77 of this database, and for a comparison between the two see Maaß & Schumann (1996). http://cfd.mace.manchester.ac.uk/ercoftac/doku.php?id=cases:case077


* Openfoam Cases

Description: https://www.cfd-online.com/Forums/openfoam-solving/155534-les-channel-flow-data-case-files-technical-report.html

Git Repo:
https://bitbucket.org/lesituu/channel_flow_data/

Excellent Discussion here:
https://www.it.uu.se/research/publications/reports/2015-014/

Openfoam files here:
https://uu.diva-portal.org/smash/record.jsf?pid=diva2%3A771689&dswid=-2515


Excellent discussion and contour plots here:

https://link.springer.com/chapter/10.1007/978-981-15-0124-1_78


BEautiful cover page here:
https://projekter.aau.dk/projekter/files/334478471/Master_Thesis_TEPE4_1000_A_Thorstensen_And_B_Dueholm.pdf
Actually, just a beautiful report
Excellent literature review



* Literature Review

| Field of investigation | Method | Findings | Reference

Mixed convection along a wavy surface
Numerical investigation
The wall heat transfer rate is found to vary accordingly to the slope of the surface. Found harmonics in the local nusselt number with a base frequency according to the wavy surface. Averaged values of nusselt number found to be universally lower than that of the flat surface
[Ghosh Moulic and Yao, 1989]

Forced convective heat transfer in periodic wavy passages
numerical simulation probably 2D
Wavy fins performed about the same in the laminar regime as flat plates and outperformed the flat plates by a factor of 2-2.5 in the wavy/transitional regime
[Wang and Vanka, 1995]



• Free-convection flat vertical plates Experimentally and based on
calculations
Determined Grashof number to be key in
determining type of flow. and obtained correlation
between Nusselt number and Grashof Number.
[Ostrach,
1952]
• Heat transfer by natural convection
with perforated fins experimental measurements 4mm perforation diameter and 45 degree geometry of
perforation found to be optimum for heat transfer.
[Pankaj
et al., 2018]
• Varies fin spaceing, perforation angle,
perforation diameter and pitch of
perforation
Includes a literature review
• Fully-developed inline square pin fin
heat exchanger
2 dimensional numerical
simulation using periodic
boundary conditions and the
MAC algorithm
Flow becomes unsteady above a critical Grashof
number
[Saha and
Chanda,
2019]
• Varies Grashof number and
geometrical pitch between 1.5 and 3
Critical Grashof number decreases with increasing fin
spacing
Nusselt number found to increase with increasing
Grashof number for a specific geometry, whereas
nusselt number decreases for an increase in pitch
with constant Grashof.



Simulation of Fully Developed Flow and Heat Transfer in Wavy Channels Using OpenFOAM

https://link.springer.com/chapter/10.1007/978-981-15-0124-1_78

https://sci-hub.st/https://doi.org/10.1007/978-981-15-0124-1_78



Evaluation of RANS turbulence
models for the simulation of channel
flow
https://www.diva-portal.org/smash/get/diva2:771689/FULLTEXT01.pdf

Turbulence production in flow over a wavy wall
https://link.springer.com/article/10.1007/BF00192670#citeas


* Outline

Introduction

Method of Transformed Coordinates was introduced to solve heat-transfer problems in the presence of irregular surfaces of all kinds.
Using this method, the boundary layer equations are mapped into a regular and stationary computational domain, and solved with a finite difference scheme.



* Estimating the first cell height for correct y+

Different turbulence models are used to capture the boundary layer in different ways.
The k-ω SST model has been widely researched, tested and is accepted as a good choice for many industrial problems, and has the robustness to resolve the viscous sublayer or the log layer.
If the first cell height is in the viscous layer, it will resolve the boundary layer by evaluating the velocity at each cell.
If the first cell is in the log layer, the software will make an empirical assumption about the viscous sublayer, referred to as using a wall function.

When resolving the viscous sublayer, a value for y+~1 is required, which will require significantly more mesh elements and effort, Figure 3, left-hand image. This will lead to higher meshing and solution times and the requirement for more computer system resources. If you determine, that is not important to resolve the viscous sublayer, i.e. to use a wall function, a value for y+ > 30 is valid; this will result in less meshing effort and computational time


The Reynolds number for a given free stream velocity $U_{\infty}$ is:
$$ Re = \frac{\rho \cdot U_{\infty} \cdot L_{boundary\,layer}}{\mu} $$





* Inflation Layer Calculator

https://www.fluidmechanics101.com/pdf/calculators.pdf

** Calculating the first cell height

Start by calculating the flow Reynolds number (Re).

$$ Re = \frac{\rho U L}{\mu} $$

where rho is the fluid density, U is the freestream velocity, L is the characteristic length of the geometry and mu is the dynamic viscosity


The Schlichting formula to obtain the skin friction coefficient Cf for fully developed turbulent flow over a flat plate is used in this calculator and is valid for $Re_x < 10^9$:

$$ C_f = [ 2 \cdot log_{10}(Re_x) - 0.65 ] ^{-2.3} \quad \mbox{for} \quad Re_x < \; 10^9 $$


With the skin coefficient, the wall shear stress is calculated from the skin friction coefficient Cf:
$$ \tau_w = C_f \cdot \frac{1}{2} \rho U_{\infty}^2 $$

The friction velocity (u_{\tau} ) can then be computed from the wall shear stress and fluid density:
$$ u_{\tau} = \sqrt{\frac{\tau_w}{\rho}} $$

Finally, the equation for y+ can be rearranged to give the height of the wall adjacent cell centroid from the wall (y_p):

y+ = \frac{\rho y_p U_{\tau} }{\mu}

yp = \frac{y+ \mu}{u_{\tau} \rho}

The distance of the cell centroid from the wall is half the height of the cell height (y_H)
y_H = 2y_p

** Calculating the final cell height of inflation layer

# For aerodynamic flows using Reynolds-averaged Navier-Stokes (RANS) turbulence modeling,

We can use empirical correlations to estimate the boundary layer thickness $\delta_{99}$.

$$ \delta_{99} = \frac{4.91 L}{ \sqrt{Re_{L}} } \quad \mbox{for} \quad Re_x < 5 * 10^5 $$
$$ \delta_{99} = \frac{0.38 L}{ {Re_{L}}^{1/5} } \quad \mbox{for} \quad Re_x > 5 * 10^5 $$

It would be ideal for the inflatino layer to cover thisssssssWe wish for the


{y = \frac{y^+ \mu}{\rho \, u^*}}
